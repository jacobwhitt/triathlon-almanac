import { apikey } from "../config";

export const fetchBestRankedAthletes = (country) =>
  fetch(
    `https://api.triathlon.org/v1/search/athletes?filters=athlete_country_name,${country}`,
    {
      method: "GET",
      headers: {
        apikey,
      },
    }
  );

export const fetchAthletesList = (userInput, page) =>
  fetch(
    `https://api.triathlon.org/v1/search/athletes?page=${page}&per_page=50&query=${userInput}`,
    {
      method: "GET",
      headers: {
        apikey,
      },
    }
  );
