export const mapAthletes = (athleteList) =>
  athleteList.map((athObj) => mapAthlete(athObj));

export const mapAthlete = ({
  athlete_title,
  athlete_profile_image,
  points_list_ranking,
  athlete_country_name,
  athlete_id,
}) => ({
  id: athlete_id,
  name: athlete_title,
  country: athlete_country_name,
  image: athlete_profile_image,
  ranking: points_list_ranking,
});
