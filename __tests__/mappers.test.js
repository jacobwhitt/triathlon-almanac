import { mapAthlete } from "../services/mappers";

describe("Mappers Test", () => {
  describe("mapAthlete", () => {
    test("Maps object correctly", () => {
      // Given
      const backendAtlete = {
        athlete_title: "Test User",
        athlete_profile_image:
          "http://www.triathlon.org/images/athlete_thumbs/mario_mola_ESP.jpg",
        points_list_ranking: 7,
      };

      // When
      const mappedAthlete = mapAthlete(backendAtlete);

      // Then
      expect(mappedAthlete.name).toBe("Test User");
      expect(mappedAthlete.image).toBe(
        "http://www.triathlon.org/images/athlete_thumbs/mario_mola_ESP.jpg"
      );
      expect(mappedAthlete.ranking).toBe(7);
    });
  });
});
