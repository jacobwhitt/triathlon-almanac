import { renderHook } from "@testing-library/react-hooks";
import { useFetchBestRanked } from "../hooks/useFetchBestRanked";
import { fetchBestRankedAthletes } from "../services/api";

jest.mock("../services/api");

const mockedFetch = fetchBestRankedAthletes;

describe("useFetchBestRanked", () => {
  test("Returns expected mapped data when the request succeeds", async () => {
    // Given
    mockedFetch.mockResolvedValue({
      json: () =>
        Promise.resolve({
          data: [
            {
              athlete_id: 11402,
              athlete_title: "Mario Mola",
              athlete_slug: "mario_mola",
              athlete_first: "Mario",
              athlete_last: "Mola",
              athlete_country_id: 270,
              athlete_gender: "male",
              athlete_yob: "1990",
              validated: true,
              athlete_profile_image:
                "http://www.triathlon.org/images/athlete_thumbs/mario_mola_ESP.jpg",
              athlete_noc: "ESP",
              athlete_country_name: "Spain",
              athlete_country_isoa2: "ES",
              athlete_listing:
                "https://www.triathlon.org/athletes/profile/11402/mario_mola",
              athlete_flag:
                "https://triathlon-images.imgix.net/images/icons/es.png",
              athlete_api_listing:
                "https://api.triathlon.org/v1/v1/athletes/11402",
              athlete_categories: [42, 539],
              points_list_ranking: 2,
              _highlightResult: {
                athlete_title: {
                  value: "Mario Mola",
                  matchLevel: "none",
                  matchedWords: [],
                },
                athlete_noc: {
                  value: "ESP",
                  matchLevel: "none",
                  matchedWords: [],
                },
                athlete_country_name: {
                  value: "Spain",
                  matchLevel: "none",
                  matchedWords: [],
                },
              },
            },
          ],
        }),
    });

    // When
    const { result, waitForNextUpdate } = renderHook(() =>
      useFetchBestRanked("Spain")
    );

    // Then
    expect(result.current.isLoading).toBeTruthy();
    await waitForNextUpdate();
    expect(result.current.isLoading).toBeFalsy();
    expect(result.current.data).toHaveLength(1);
    expect(result.current.data[0].name).toBe("Mario Mola");
    expect(result.current.hasError).toBeFalsy();
    expect(mockedFetch).toHaveBeenCalledTimes(1);
    expect(mockedFetch).toHaveBeenCalledWith("Spain");
  });

  test("setHasError is true when the request fails", async () => {
    // Given
    mockedFetch.mockRejectedValue(new Error());

    // When
    const { result, waitForNextUpdate } = renderHook(() =>
      useFetchBestRanked("Spain")
    );
    await waitForNextUpdate();

    // Then
    expect(result.current.hasError).toBeTruthy();
  });
});
