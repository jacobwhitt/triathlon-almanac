import React from "react";
import { TouchableWithoutFeedback, Keyboard } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import SearchHeader from "../components/SearchHeader/SearchHeader";
import { BestRankedAthletes } from "../components/BestRankedAthletes/BestRankedAthletes";

const HomeScreen = ({ route, navigation }) => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: "#fff",
      }}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <>
          <SearchHeader navigation={navigation} route={route} />
          <BestRankedAthletes country="Spain" />
        </>
      </TouchableWithoutFeedback>
    </SafeAreaView>
  );
};

export default HomeScreen;
