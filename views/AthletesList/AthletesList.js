import React from "react";
import {
  FlatList,
  Keyboard,
  SafeAreaView,
  Text,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { useFetchAthletesList } from "../../hooks/useFetchAthletesList";
import { AthleteItem } from "../../components/AthleteItem/AthleteItem";
import { Loading } from "../../components/Loading/Loading";
import { Pagination } from "../../components/Pagination/Pagination";
import { SearchError } from "../../components/SearchError/SearchError";
import styles from "./styles";
import SearchHeader from "../../components/SearchHeader/SearchHeader";

const AthletesList = ({ route, navigation }) => {
  const { userInput } = route.params;

  const {
    isLoading,
    page: currentPage,
    lastPage: totalPages,
    nextPage: next,
    prevPage: prev,
    jumpToPage: jump,
    data: athletes,
    hasError,
  } = useFetchAthletesList(userInput, currentPage);

  const refresh = () => {
    console.log("refreshing!");
    isLoading(true);
  };

  return isLoading ? (
    <Loading />
  ) : (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <SafeAreaView style={styles.container}>
        {athletes.length > 0 ? (
          <>
            <SearchHeader
              refresh={refresh}
              navigation={navigation}
              route={route}
            />
            <Pagination
              current={currentPage}
              totalPages={totalPages}
              next={next}
              prev={prev}
              jump={jump}
            />
            <Text style={styles.text}>Resultos de {userInput}:</Text>
            <FlatList
              contentContainerStyle={styles.flatList}
              numColumns={1}
              data={athletes}
              renderItem={({ item: props }) => (
                <View style={styles.gridWrapper}>
                  <AthleteItem {...props} />
                </View>
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          </>
        ) : (
          <>
            <SearchHeader
              isLoading={isLoading}
              navigation={navigation}
              route={route}
            />
            <SearchError />
          </>
        )}
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

export default AthletesList;
