import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    width: "100%",
  },
  flatList: {
    flexGrow: 1,
    width: "90%",
    marginHorizontal: "5%",
    borderWidth: 1,
    borderColor: "teal",
    borderRadius: 10,
  },
  text: {
    fontSize: 20,
    marginBottom: 5,
    color: "teal",
    textAlign: "center",
  },
});
