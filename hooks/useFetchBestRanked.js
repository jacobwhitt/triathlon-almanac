import { useEffect, useState } from "react";
import { fetchBestRankedAthletes } from "../services/api";
import { mapAthletes } from "../services/mappers";

export const useFetchBestRanked = (country) => {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const [hasError, setHasError] = useState(false);

  const fetchAthletes = () => {
    fetchBestRankedAthletes(country)
      .then((resp) => resp.json())
      .then(({ data }) => setData(mapAthletes(data)))
      .catch(() => setHasError(true))
      .finally(() => setIsLoading(false));
  };

  useEffect(() => {
    fetchAthletes();
  }, []);

  return { isLoading, data, hasError };
};
