import { useEffect, useState } from "react";
import { fetchAthletesList } from "../services/api";
import { mapAthletes } from "../services/mappers";

export const useFetchAthletesList = (userInput) => {
  const [isLoading, setIsLoading] = useState(true); // render ActivityIndicator while fetching
  const [data, setData] = useState([]); // list of athletes fetched
  const [hasError, setHasError] = useState(false);
  const [page, setPage] = useState(1); // current page
  const [lastPage, setLastPage] = useState(20); // number of total pages needed to display list of athletes based on results per page

  // use pagination buttons to control flatList pages
  const nextPage = () => {
    console.log("nextPage in hook");
    setPage(page + 1);
  };
  const prevPage = () => {
    console.log("prevPage in hook");
    setPage(page - 1);
  };
  const jumpToPage = (num) => {
    console.log("jumpToPage in hook");
    setPage(num);
  };

  useEffect(() => {
    fetchAthletes();
  }, [page]);

  const fetchAthletes = () => {
    fetchAthletesList(userInput, page)
      .then((resp) => resp.json())
      .then(({ data, last_page, total }) => {
        last_page < 20 ? setLastPage(last_page) : setLastPage(20);
        setData(mapAthletes(data));
      })
      .catch(() => setHasError(true))
      .finally(() => setIsLoading(false));
  };

  return {
    isLoading,
    data,
    hasError,
    page,
    lastPage,
    nextPage,
    prevPage,
    jumpToPage,
  };
};
