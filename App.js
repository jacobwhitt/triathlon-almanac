import React from "react";
import { Dimensions, StatusBar, StyleSheet } from "react-native";
import Home from "./views/Home";
import AthletesList from "./views/AthletesList/AthletesList";
import SearchHeader from "./components/SearchHeader/SearchHeader";
// import AthleteDetails from "./views/AthleteDetails";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
const Stack = createNativeStackNavigator();

const { width, height } = Dimensions.get("window");

export default function App() {
  StatusBar.setBarStyle("dark-content", true);

  return (
    <>
      <NavigationContainer style={styles.container}>
        <Stack.Navigator
          initialRouteName="Home"
          screenOptions={{ headerShown: false, gestureEnabled: true }}
        >
          <Stack.Screen name="Home">
            {(props) => <Home {...props} />}
          </Stack.Screen>

          <Stack.Screen name="AthletesList">
            {(props) => <AthletesList {...props} />}
          </Stack.Screen>

          <Stack.Screen name="SearchHeader">
            {(props) => <SearchHeader {...props} />}
          </Stack.Screen>

          {/* <Stack.Screen name="AthletesDetails">
            {(props) => <AthletesDetails {...props} />}
          </Stack.Screen> */}

          {/* <Stack.Screen name="AthleteDetails" component={AthleteDetails} /> */}
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height,
  },
  textInput: {
    width: 200,
    height: 40,
    borderWidth: StyleSheet.hairlineWidth,
  },
});
