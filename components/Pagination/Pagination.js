import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import styles from "./styles";

export const Pagination = ({ current, totalPages, next, prev, jump }) => {
  const tempArr = [];
  const edges = 3;

  console.log("current:", current, "total:", totalPages);

  const renderBtns = () => {
    for (let i = 1; i <= totalPages; i++) {
      tempArr.push(
        <TouchableOpacity
          style={[styles.btn, i === current && styles.activeBtn]}
          onPress={() => {
            jump(i);
          }}
          key={i}
        >
          <Text style={[styles.text, i === current && styles.activeText]}>
            {i}
          </Text>
        </TouchableOpacity>
      );
    }
    return tempArr;
  };
  renderBtns();

  const displayBtns = () => {
    if (totalPages < 5) {
      return tempArr;
    }

    if (totalPages > 5 && current === 1) {
      return tempArr.slice(0, edges + 1);
    } else if (current <= edges && current < totalPages) {
      return tempArr.slice(current - current, edges + 1);
    } else if (current > edges && current < totalPages) {
      return tempArr.slice(current - edges, current + (edges - 1));
    } else if (current === totalPages) {
      return tempArr.slice(current - (edges + 1), current);
    }
  };

  return (
    <View style={styles.container}>
      {current >= 3 ? (
        <TouchableOpacity
          style={styles.prevBtn}
          onPress={() => {
            jump(1);
          }}
        >
          <Text style={styles.text}>First</Text>
        </TouchableOpacity>
      ) : (
        current >= 2 && (
          <TouchableOpacity
            style={styles.prevBtn}
            onPress={() => {
              prev();
            }}
          >
            <Text style={styles.text}>Prev</Text>
          </TouchableOpacity>
        )
      )}

      {displayBtns()}

      {current !== totalPages && (
        <TouchableOpacity
          style={styles.nextBtn}
          onPress={() => {
            next();
          }}
        >
          <Text style={styles.text}>Next</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};
