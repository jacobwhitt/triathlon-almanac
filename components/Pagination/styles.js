import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    marginBottom: 10,
    justifyContent: "center",
  },
  activeBtn: {
    width: 40,
    height: 30,
    // marginRight: 10,
    borderRadius: 10,
    backgroundColor: "#ffc107",
    justifyContent: "center",
  },
  activeText: {
    color: "#000",
  },
  btn: {
    width: 40,
    height: 30,
    marginRight: 5,
    borderRadius: 10,
    backgroundColor: "#007bff",
    justifyContent: "center",
  },
  nextBtn: {
    width: 50,
    height: 30,
    marginRight: 10,
    borderRadius: 10,
    backgroundColor: "#28a745",
    justifyContent: "center",
  },
  prevBtn: {
    width: 50,
    height: 30,
    marginRight: 10,
    borderRadius: 10,
    backgroundColor: "#dc3545",
    justifyContent: "center",
  },
  text: {
    fontSize: 12,
    color: "#fff",
    textAlign: "center",
  },
});
