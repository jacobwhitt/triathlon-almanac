import { StyleSheet } from "react-native";

export default StyleSheet.create({
  text: {
    fontSize: 20,
    marginBottom: 5,
    color: "teal",
    textAlign: "center",
  },
  error: {
    marginTop: 50,
    marginHorizontal: "10%",
    width: "80%",
  },
  errorText: {
    fontSize: 20,
    fontWeight: "800",
    color: "teal",
    marginBottom: 5,
    textAlign: "center",
  },
});
