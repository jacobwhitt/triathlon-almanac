import React from "react";
import { Text, View } from "react-native";
import styles from "./styles";

export const SearchError = () => (
  <View style={styles.error}>
    <Text style={styles.errorText}>
      There don't seem to be any matching results for that name.
    </Text>
  </View>
);
