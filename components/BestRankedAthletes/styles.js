import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "column",
    justifyContent: "center",
    width: "100%",
    padding: 0,
  },
  scrollView: {
    flexGrow: 1,
    width: "90%",
    marginHorizontal: "5%",
    borderWidth: 1,
    borderWidth: 1,
    borderColor: "teal",
    borderRadius: 10,
    alignItems: "center",
  },
  text: {
    fontSize: 20,
    marginBottom: 5,
    textAlign: "center",
  },
});
