import React from "react";
import { SafeAreaView, ScrollView, Text } from "react-native";
import { useFetchBestRanked } from "../../hooks/useFetchBestRanked";
import { AthleteItem } from "../AthleteItem/AthleteItem";
import { Loading } from "../Loading/Loading";
import styles from "./styles";

export const BestRankedAthletes = ({ country }) => {
  const { isLoading, data: athletes, hasError } = useFetchBestRanked(country);

  return isLoading ? (
    <Loading />
  ) : (
    <SafeAreaView style={styles.container}>
      <Text style={styles.text}>Top 10 Triatletas Españoles</Text>
      <ScrollView contentContainerStyle={styles.scrollView}>
        {athletes.map((props, i) => (
          <AthleteItem {...props} key={`${i}`} />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};
