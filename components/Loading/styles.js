import { StyleSheet } from "react-native";

export default StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 40,
    marginBottom: 10,
    color: "dodgerblue",
  },
});
