import React from "react";
import { ActivityIndicator, Text, View } from "react-native";
import styles from "./styles";

export const Loading = () => {
  return (
    <View style={styles.loading}>
      <Text style={styles.text}>Loading...</Text>
      <ActivityIndicator animating={true} size="large" color="dodgerblue" />
    </View>
  );
};
