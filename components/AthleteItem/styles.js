import { StyleSheet } from "react-native";

export default StyleSheet.create({
  athleteItem: {
    flexDirection: "row",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "teal",
    borderRadius: 20,
    marginVertical: 10,
    marginHorizontal: "5%",
    width: "90%",
    height: 100,
    alignItems: "center",
  },
  athleteItemLeft: {
    marginHorizontal: 10,
  },
  athleteItemRight: {
    flexDirection: "column",
    justifyContent: "space-evenly",
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 20,
  },
  text: {
    fontSize: 12,
    lineHeight: 30,
  },
});
