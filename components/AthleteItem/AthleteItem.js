import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import styles from "./styles";
const missing = require("../../assets/missing.jpg");

export const AthleteItem = ({ name, image, ranking, country }) => (
  <TouchableOpacity
    style={styles.athleteItem}
    onPress={() => {
      console.log(`${name} pressed!`);
    }}
  >
    <View style={styles.athleteItemLeft}>
      <Image
        source={image === null ? missing : { uri: image }}
        style={styles.image}
      />
    </View>

    <View style={styles.athleteItemRight}>
      <Text style={styles.text}>Nombre: {name}</Text>
      <Text style={styles.text}>País: {country}</Text>
      <Text style={styles.text}>
        Ranking: {ranking === null ? "N/A" : ranking}
      </Text>
    </View>
  </TouchableOpacity>
);
