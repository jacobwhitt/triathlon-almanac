import React, { useCallback, useState } from "react";
import { TextInput, View, Keyboard } from "react-native";
import { debounce } from "lodash";

import styles from "./styles";

const SearchHeader = (props) => {
  const [prevInputValue, setPrevInputValue] = useState("");

  const onChangeText = (text) => {
    Keyboard.dismiss();
    console.log("prevInput:", prevInputValue);
    console.log("currentInput:", text);

    if (!text) {
      return;
    } else {
      setPrevInputValue(text.trim());
      return props.route.name === "AthletesList"
        ? props.navigation.push("AthletesList", {
            userInput: text,
          })
        : props.navigation.navigate("AthletesList", {
            userInput: text,
          });
    }
  };

  const debouncedOnChange = useCallback(debounce(onChangeText, 500), []);

  return (
    <View style={styles.searchBar}>
      <TextInput
        onChangeText={debouncedOnChange}
        placeholder="Busca a un/a atleta..."
        placeholderTextColor="teal"
        returnKeyType={"next"}
        underlineColorAndroid="transparent"
        style={styles.textInput}
      />
    </View>
  );
};

export default SearchHeader;
