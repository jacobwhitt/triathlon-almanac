import { StyleSheet } from "react-native";

export default StyleSheet.create({
  searchBar: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 10,
  },
  textInput: {
    width: "90%",
    height: 40,
    borderWidth: 1,
    borderColor: "teal",
    borderRadius: 10,
    paddingLeft: 10,
  },
  title: {
    fontSize: 40,
  },
  text: {
    marginRight: 20,
  },
});
